/**
 * Genesis Sample entry point.
 *
 * @package GenesisSample\JS
 * @author  StudioPress
 * @license GPL-2.0+
 */

var genesisSample = ( function( $ ) {
    'use strict';

    // set the variables.
        var $header = $('.site-header'),
        $siteContainer = $('.site-container'),
        $hsToggle = $('.toggle-header-search'),
        $hsWrap = $('#header-search-container'),
        $hsInput = $hsWrap.find('input[type="search"]'),
    

    /**
     * Initialize Genesis Sample.
     *
     * Internal functions to execute on document load can be called here.
     *
     * @since 2.6.0
     */
    init = function() {

       // Make sure JS class is added.
        $('body').addClass('js');
        
    };

    // Run on page scroll.
    $( window ).scroll( function() {

        // Toggle header class after threshold point.
        if ( $( document ).scrollTop() > 10 ) {
            $header.addClass( 'scrolled' );
        } else {
            $header.removeClass( 'scrolled' );
        }

    });



    // Handler for click a show/hide button.
    $hsToggle.on('click', function (event) {

        event.preventDefault();

        if ($(this).hasClass('close')) {
            hideSearch();
        } else {
            showSearch();
        }

    });

    // Handler for pressing show/hide button.
    $hsToggle.on('keydown', function (event) {

        // If tabbing from toggle button, and search is hidden, exit early.
        if (event.keyCode === 9 && !$header.hasClass('search-visible')) {
            return;
        }

        event.preventDefault();
        handleKeyDown(event);

    });


    // Escape key to exit search
    $hsInput.keyup(function(e){
        if(e.keyCode == 27) {
            hideSearch();
        }
    });

    // Handler for tabbing out of the search bar when focused.
    $hsInput.on('keydown', function (event) {

        if (event.keyCode === 9) {
            hideSearch(event.target);
        }

    });

    // Helper function to show the search form.
    function showSearch() {

        $header.addClass('search-visible');
        $hsWrap.fadeIn('slow').find('input[type="search"]').focus();
        $hsToggle.attr('aria-expanded', true);

    }

    // Helper function to hide the search form.
    function hideSearch() {

        $hsWrap.fadeOut('slow');
        $hsToggle.attr('aria-expanded', false);
        $header.removeClass('search-visible');

    }

    // Keydown handler function for toggling search field visibility.
    function handleKeyDown(event) {

        // Enter/Space, respectively.
        if (event.keyCode === 13 || event.keyCode === 32) {

            event.preventDefault();

            if ($(event.target).hasClass('close')) {
                hideSearch();
            } else {
                showSearch();
            }

        }

    }
    
    $(".menu-item").mouseover(function(){
        $(".menu-item").css("opacity",0.8);
        $(this).css("opacity",1);
    });
    
    $(".menu-item").mouseleave(function(){
        $(".menu-item").css("opacity",1);
    });
    
    // Expose the init function only.
    return {
        init: init 
    };

})( jQuery );

jQuery( window ).on( 'load', genesisSample.init );