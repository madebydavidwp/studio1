<?php
/**
 * This file contains widget areas for the Genesis Starter Theme.
 *
 * @package   GenesisStarter
 * @link      https://seothemes.com/themes/genesis-starter
 * @author    SEO Themes
 * @copyright Copyright © 2017 SEO Themes
 * @license   GPL-2.0+
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {

	die;

}

// Registers front-page widget areas.
for ( $i = 1; $i <= 6; $i++ ) {
    genesis_register_widget_area(
        array(
            'id'          => "front-page-{$i}",
            'name'        => __( "Front Page {$i}", 'genesis-sample' ),
            'description' => __( "This is the front page {$i} section.", 'genesis-sample' ),
        )
    );
}

//* Register widget areas
genesis_register_sidebar( array(
	'id'          => 'before-footer',
	'name'        => __( 'Before Footer', 'genesis-sample' ),
	'description' => __( 'This is the are directly above the footer', 'genesis-sample' ),
) );