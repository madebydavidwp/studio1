<?php
/**
 * Eviebest Pro
 *
 * This file adds the header icon menu to the Eviebest Pro Theme.
 *
 * @package   EviebestPro
 * @link      https://seothemes.com/themes/eviebest-pro
 * @author    SEO Themes
 * @copyright Copyright © 2017 SEO Themes
 * @license   GPL-2.0+
 */

add_action( 'genesis_header', 'custom_get_header_search_toggle', 12 );
/**
 * Outputs the header search form toggle button.
 */
function custom_get_header_search_toggle() {
    printf(
        '<a href="#header-search-wrap" aria-controls="header-search-wrap" aria-expanded="false" role="button" class="toggle-header-search"><span class="screen-reader-text">%s</span><svg class="ion ion-android-search xmlns=" http:="" www.w3.org="" 2000="" svg"="" xlink="http://www.w3.org/1999/xlink" aria-hidden="true" width="20px" height="20px" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 512 512"><path d="M344.5 298c15-23.6 23.8-51.6 23.8-81.7 0-84.1-68.1-152.3-152.1-152.3C132.1 64 64 132.2 64 216.3c0 84.1 68.1 152.3 152.1 152.3 30.5 0 58.9-9 82.7-24.4l6.9-4.8L414.3 448l33.7-34.3-108.5-108.6 5-7.1zm-43.1-166.8c22.7 22.7 35.2 52.9 35.2 85s-12.5 62.3-35.2 85c-22.7 22.7-52.9 35.2-85 35.2s-62.3-12.5-85-35.2c-22.7-22.7-35.2-52.9-35.2-85s12.5-62.3 35.2-85c22.7-22.7 52.9-35.2 85-35.2s62.3 12.5 85 35.2z" fill="#626262"></path><rect x="0" y="0" width="512" height="512" fill="rgba(0, 0, 0, 0)"></rect></svg></span></a>',
        __( 'Show Search', 'genesis-sample' )
    );
}

add_action( 'genesis_header', 'custom_do_header_search_form', 14 );
/**
 * Outputs the header search form.
 */
function custom_do_header_search_form() {
    $button = sprintf(
        '<a href="#" role="button" aria-expanded="false" aria-controls="header-search-wrap" class="toggle-header-search close"><span class="screen-reader-text">%s</span><svg class="ionicons ion-android-close" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" width="20px" height="20px" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 512 512"><path d="M405 136.798L375.202 107 256 226.202 136.798 107 107 136.798 226.202 256 107 375.202 136.798 405 256 285.798 375.202 405 405 375.202 285.798 256z" fill="#626262"></path><rect x="0" y="0" width="512" height="512" fill="rgba(0, 0, 0, 0)"></rect></svg></span></a>',
        __( 'Hide Search', 'genesis-sample' )
    );

    printf(
        '<div id="header-search-container" class="header-search-container">%s %s</div>',
        get_search_form( false ),
        $button
    );
}
