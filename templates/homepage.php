<?php
/**
 * Eviebest Pro
 *
 * Template Name: Home
 *
 * This file adds the Home page template to the Eviebest Pro Theme.
 *
 * @package   EviebestPro
 * @link      https://seothemes.com/themes/eviebest-pro
 * @author    SEO Themes
 * @copyright Copyright © 2017 SEO Themes
 * @license   GPL-2.0+
 */

// Force full-width-content layout.
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

// Remove default loop.
remove_action( 'genesis_loop', 'genesis_do_loop' );

// Add widget areas
add_action( 'genesis_loop', 'eviebest_pro_front_page_loop' );


/**
 * Front page content.
 *
 * @since  0.1.0
 *
 * @return void
 */
function eviebest_pro_front_page_loop() {
    
        eviebest_pro_do_widget( 'front-page-1' );
		eviebest_pro_do_widget( 'front-page-2' );
		eviebest_pro_do_widget( 'front-page-3' );
		eviebest_pro_do_widget( 'front-page-4' );
		eviebest_pro_do_widget( 'front-page-5' );
		eviebest_pro_do_widget( 'front-page-6' );
		eviebest_pro_do_widget( 'front-page-7' );
    
}

/**
 * Helper function to handle outputting widget markup and classes.
 *
 * @since 0.1.0
 *
 * @param string $id The id of the widget area.
 */
function eviebest_pro_do_widget( $id ) {

	genesis_widget_area( $id, array(
		'before' => "<div id=\"$id\" class=\"section $id\"><div class=\"wrap\">",
		'after'  => '</div></div>',
	) );

}

// Run the Genesis loop.
genesis();
